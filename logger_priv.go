package logger

type logger struct {
	root   *root
	tags   []string
	fields []keyval
	data   []keyval
}

func (l *logger) spawn() *logger {
	var spawn *logger

	spawn = &logger{
		root:   l.root,
		fields: l.fields,
		tags:   l.tags,
		data:   l.data,
	}

	return spawn
}

func (l *logger) addRawFields(fields map[string]interface{}) {
	for k, v := range fields {
		if !l.root.options.DedupFields || !l.containsField(k) {
			l.fields = append(l.fields, newKV(k, v))
		}
	}
}

func (l *logger) addFields(fields ...keyval) {
	if len(fields) == 0 {
		return
	}

	for _, field := range fields {
		if !l.root.options.DedupFields || !l.containsField(field.name) {
			l.fields = append(l.fields, field)
		}
	}
}

func (l *logger) addTags(tags ...string) {
	if len(tags) == 0 {
		return
	}

	for _, t := range tags {
		if !l.root.options.DedupTags || !l.containsTag(t) {
			l.tags = append(l.tags, t)
		}
	}
}

func (l *logger) addRawData(data map[string]interface{}) {
	if data == nil || len(data) == 0 {
		return
	}

	if l.data == nil {
		l.data = make([]keyval, 0, len(data))
	}

	for k, v := range data {
		l.data = append(l.data, newKV(k, v))
	}
}

func (l *logger) write(msg string) {
	l.root.write(msg, l.tags, l.fields, l.data)
}

func (l *logger) containsField(needle string) bool {
	if l.fields == nil {
		return false
	}

	for _, field := range l.fields {
		if field.name == needle {
			return true
		}
	}
	return false
}

func (l *logger) containsTag(needle string) bool {
	if l.tags == nil {
		return false
	}

	for _, tag := range l.tags {
		if tag == needle {
			return true
		}
	}
	return false
}
