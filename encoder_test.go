package logger

import (
	"encoding/json"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFormatValue(t *testing.T) {
	var str []byte

	str, _ = formatValue(`"te":"st"`)
	assert.Equal(t, []byte(`"\"te\":\"st\""`), str, "format failed for quoted string")

	str, _ = formatValue(`\"te":\"st"`)
	assert.Equal(t, []byte(`"\\\"te\":\\\"st\""`), str, "format failed for quoted string type 2")

	str, _ = formatValue([]string{`"a"`, `b`})
	assert.Equal(t, []byte(`["\"a\"","b"]`), str, "format failed for array: %s")

	str, _ = formatValue(123)
	assert.Equal(t, []byte(`123`), str, "format failed for number")

	str, _ = formatValue("{an amazing test of power}")
	assert.Equal(t, []byte(`"{an amazing test of power}"`), str, "format failed for string with special chars")

	str, _ = formatValue("an amazing test of ultra power")
	assert.Equal(t, []byte(`"an amazing test of ultra power"`), str, "format failed for quoted string")

	str, _ = formatValue("")
	assert.Equal(t, []byte(`""`), str, "failed for empty string")

	str, _ = formatValue("aþø→")
	assert.Exactly(t, str, []byte(`"aþø→"`))

	str, _ = formatValue(1)
	assert.Exactly(t, str, []byte(`1`))

	str, _ = formatValue(10.2312341)
	assert.Exactly(t, str, []byte(`10.2312341`))

	var sample = struct {
		Name   string
		Number int
	}{
		Name:   "john",
		Number: 1,
	}
	str, _ = formatValue(sample)
	assert.Exactly(t, str, []byte(`{"Name":"john","Number":1}`))

	str, _ = formatValue(json.RawMessage(`{"field":"value""}`))
	assert.Exactly(t, str, []byte(`"there was a problem formatting field: json: error calling MarshalJSON for type json.RawMessage: invalid character '\"' after object key:value pair"`))
}

var benchBSliceOutput []byte

func BenchmarkEncodeString(b *testing.B) {
	b.Run("encode", func(b *testing.B) {
		b.ResetTimer()
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				var enc = _jep.Get()
				enc.Encode("asdw\bçdawdawd")
				benchBSliceOutput = enc.Done()
				_jep.Put(enc)
			}
		})
	})
	b.Run("marshal", func(b *testing.B) {
		b.ResetTimer()
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				benchBSliceOutput, _ = json.Marshal("asdw\bçdawdawd")
			}
		})
	})
}

func BenchmarkEncodeInt(b *testing.B) {
	var getint = func() interface{} {
		return 100
	}
	b.Run("strconv", func(b *testing.B) {
		b.ResetTimer()
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				switch v := getint().(type) {
				case int:
					benchBSliceOutput = strconv.AppendInt(make([]byte, 0), int64(v), 10)
				}
			}
		})
	})
	b.Run("encode", func(b *testing.B) {
		b.ResetTimer()
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				var enc = _jep.Get()
				enc.Encode(100)
				benchBSliceOutput = enc.Done()
				_jep.Put(enc)
			}
		})
	})
	b.Run("formatValue", func(b *testing.B) {
		b.ResetTimer()
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				benchBSliceOutput, _ = formatValue(100)
			}
		})
	})
}

func BenchmarkStringSlice(b *testing.B) {
	getslice := func() []string {
		return []string{"one", "two", "three", "four"}
	}
	b.Run("formatValue", func(b *testing.B) {
		b.ResetTimer()
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				benchBSliceOutput, _ = formatValue(getslice())
			}
		})
	})
	b.Run("write", func(b *testing.B) {
		b.ResetTimer()
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				var enc = _jep.Get()
				enc.Encode(getslice())
				benchBSliceOutput = enc.Done()
				_jep.Put(enc)
			}
		})
	})
}
