package logger

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGlobalLoggerInterface(t *testing.T) {
	b := &bytes.Buffer{}
	Reconfigure(ConfigWriter(b))

	DebugMode(true)
	Debug().Write("test")
	assert.Contains(t, b.String(), `"msg":"test"`)
	b.Reset()
	Debug().Write("test %s", "a")
	assert.Contains(t, b.String(), `"msg":"test a"`)
	b.Reset()
	Debug().WithData(KV{"field": "sample"}).Write("test")
	assert.Contains(t, b.String(), `"msg":"test"`)
	assert.Contains(t, b.String(), `"data":{"field":"sample"}`)
	b.Reset()

	DebugMode(false)
	Write("test")
	assert.Contains(t, b.String(), "test")
	b.Reset()
	Write("test %s", "a")
	assert.Contains(t, b.String(), "test a")
	b.Reset()
	WithData(KV{"field": "sample"}).Write("test")
	assert.Contains(t, b.String(), `"msg":"test"`)
	assert.Contains(t, b.String(), `"data":{"field":"sample"}`)
	b.Reset()

	Write("test")
	assert.Contains(t, b.String(), `"msg":"test"`)
	b.Reset()
	Spawn().WithTags("tag").Write("test")
	assert.Contains(t, b.String(), `"msg":"test"`)
	assert.Contains(t, b.String(), `"tags":["tag"]`)
	b.Reset()
	Spawn().WithData(KV{"key": "val"}).Write("test %s", "name")
	assert.Contains(t, b.String(), `"msg":"test name"`)
	assert.Contains(t, b.String(), `"data":{"key":"val"}`)
	b.Reset()
	Spawn().WithTags("tag").WithData(KV{"key": "val"}).Write("test %s", "name")
	assert.Contains(t, b.String(), `"msg":"test name"`)
	assert.Contains(t, b.String(), `"data":{"key":"val"}`)
	assert.Contains(t, b.String(), `"tags":["tag"]`)
	b.Reset()
}

func TestGlobalWriterInterface(t *testing.T) {
	b := &bytes.Buffer{}
	Reconfigure(ConfigWriter(b))

	DebugMode(true)
	l := Spawn()
	l.Debug().Write("test")
	assert.Contains(t, b.String(), `"msg":"test"`)
	b.Reset()
}

func TestGlobalSpawns(t *testing.T) {
	b := &bytes.Buffer{}
	Reconfigure(ConfigWriter(b))

	tmp := Spawn().WithData(KV{"a": "a", "b": "b"})
	tmp.Write("test")
	assert.Contains(t, b.String(), `"msg":"test"`)
	assert.Contains(t, b.String(), `"a":"a"`)
	assert.Contains(t, b.String(), `"b":"b"`)
	b.Reset()

	tmp = Spawn().WithFields(KV{"a": "a", "b": "b"})
	tmp.Write("test")
	assert.Contains(t, b.String(), `"a":"a"`)
	assert.Contains(t, b.String(), `"b":"b"`)
	assert.Contains(t, b.String(), `"msg":"test"`)
	b.Reset()

	tmp = Spawn().WithTags("one", "two", "three")
	tmp.Write("test")
	assert.Contains(t, b.String(), `"tags":["one","two","three"]`)
	b.Reset()
}

func TestSpawnMute(t *testing.T) {
	SpawnMute()
}
