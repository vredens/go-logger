package logger

type dummy struct{}

// Debug marks the logger as in debug mode. Any spawned loggers will be marked as debug also.
func (logger *dummy) Debug() Logger {
	return logger
}

// WithTags creates a new logger and adds the provided tags.
func (logger *dummy) WithTags(tags ...string) Logger {
	return logger
}

// WithFields spawns a new logger entry adding the provided key value map as fields.
func (logger *dummy) WithFields(fields map[string]interface{}) Logger {
	return logger
}

// WithField creates a new logger and adds the provided field.
func (logger *dummy) WithField(key string, value interface{}) Logger {
	return logger
}

// WithData spawns a new logger then adds the provided data.
func (logger *dummy) WithData(data map[string]interface{}) Logger {
	return logger
}

// Write writes a log entry.
func (logger *dummy) Write(format string, args ...interface{}) {}
