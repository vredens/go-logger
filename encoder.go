package logger

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"sync"
)

var _jep = &jsonEncoderPool{
	pool: sync.Pool{
		New: func() interface{} {
			var buf = bytes.NewBuffer(make([]byte, 0, 512))
			var jenc = json.NewEncoder(&jsonIOWriter{buf})
			jenc.SetEscapeHTML(false)
			return &jsonEncoder{
				buf: buf,
				enc: jenc,
			}
		},
	},
}

type jsonIOWriter struct {
	buf *bytes.Buffer
}

func (jw *jsonIOWriter) Write(data []byte) (int, error) {
	// strip the trailing \n
	return jw.buf.Write(data[:len(data)-1])
}

type jsonEncoderPool struct {
	pool sync.Pool
}

func (jep *jsonEncoderPool) Get() *jsonEncoder {
	jenc := jep.pool.Get().(*jsonEncoder)
	jenc.buf.Reset()
	return jenc
}

func (jep *jsonEncoderPool) Put(jenc *jsonEncoder) {
	jep.pool.Put(jenc)
}

type jsonEncoder struct {
	buf *bytes.Buffer
	enc *json.Encoder
}

// Encode will encode the provided value as JSON.
func (jenc *jsonEncoder) Encode(val interface{}) error {
	if err := jenc.enc.Encode(val); err != nil {
		jenc.buf.WriteString(fmt.Sprintf("%q", "there was a problem formatting field: "+err.Error()))
		return err
	}
	return nil
}

func (jenc *jsonEncoder) Write(buf []byte) {
	jenc.buf.Write(buf)
}

// WriteByte to the buffer.
func (jenc *jsonEncoder) WriteByte(b byte) {
	jenc.buf.WriteByte(b)
}

// WriteString writes the provided string to the underlying buffer.
func (jenc *jsonEncoder) WriteString(str string) {
	jenc.buf.WriteString(str)
}

// WriteKeyVals writes into the underlying buffer the passed keyvals. This does not add '{' or '}'.
func (jenc *jsonEncoder) WriteKeyVals(kvs []keyval) {
	jenc.buf.WriteByte('"')
	jenc.buf.WriteString(kvs[0].name)
	jenc.buf.Write([]byte(`":`))
	jenc.buf.Write(kvs[0].data)
	for _, kv := range kvs[1:] {
		jenc.buf.Write([]byte(`,"`))
		jenc.buf.WriteString(kv.name)
		jenc.buf.Write([]byte(`":`))
		jenc.buf.Write(kv.data)
	}
}

// Done will finish the encoding process and return the encoded JSON as a byte slice.
// It is not safe to keep encoding after calling Done.
func (jenc *jsonEncoder) Done() []byte {
	var out = make([]byte, jenc.buf.Len())
	copy(out, jenc.buf.Bytes())

	return out
}

// Flush writes the encoder's buffer into the provided writer.
func (jenc *jsonEncoder) Flush(w io.Writer) (int64, error) {
	return jenc.buf.WriteTo(w)
}

func formatValue(value interface{}) ([]byte, error) {
	val, err := json.Marshal(value)
	if err != nil {
		return []byte(fmt.Sprintf("%q", "there was a problem formatting field: "+err.Error())), err
	}
	return val, nil
}
