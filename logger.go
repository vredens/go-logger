package logger

import (
	"fmt"
)

// Spawn creates a new instance of a Logger from the root logger.
func (r *root) Spawn() Logger {
	return r.spawn()
}

// Debug ...
func (l *logger) Debug() Logger {
	if !l.root.isDebugActive() {
		return &gDummy
	}
	return l
}

// WithData creates a new instance of a logger with the provided data.
func (l *logger) WithData(data map[string]interface{}) Logger {
	var spawn = l.spawn()

	spawn.addRawData(data)

	return spawn
}

// WithFields creates a new instance of a logger with the provided fields (as a map).
func (l *logger) WithFields(fields map[string]interface{}) Logger {
	var spawn = l.spawn()

	spawn.addRawFields(fields)

	return spawn
}

// WithField creates a new logger with the provided Fields.
func (l *logger) WithField(key string, value interface{}) Logger {
	var spawn = l.spawn()

	spawn.addFields(newKV(key, value))

	return spawn
}

// WithTags creates a new instance of a logger with the provided tags.
func (l *logger) WithTags(tags ...string) Logger {
	var spawn = l.spawn()

	spawn.addTags(tags...)

	return spawn
}

// Write ...
func (l *logger) Write(format string, args ...interface{}) {
	if len(args) > 0 {
		l.write(fmt.Sprintf(format, args...))
		return
	}
	l.write(format)
}
