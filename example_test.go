package logger_test

import (
	logger "gitlab.com/vredens/go-logger/v2"
)

func Example() {
	var log = logger.Spawn().WithTags("user", "account").WithFields(map[string]interface{}{"component": "mypackage"})

	// logging with the default global logger
	logger.Debug().Write("This is a debug of %s", "hello world")

	// log with our function level logger
	log.Write("this is a message")

	// logging with extra data
	log.WithData(logger.KV{"id": 10, "age": 50, "name": "john smith"}).WithTags(logger.WARN).Write("A message with structured data")

	// create a new small scope logger with some fields
	userLog := log.WithFields(logger.KV{"user_type": "me", "user_id": 1234})

	// logging with the new logger
	userLog.Debug().WithData(logger.KV{"kiss": "is simple"}).Write("here's a log with some extra data")

	// change the logging format and min log level
	logger.DebugMode(true)

	// Log an error message with extra tags and fields
	userLog.WithTags("critical").WithFields(logger.KV{"ctx_id": 666}).WithTags(logger.ERROR).Write("the devil is around here!")

	// Add stack (set) of callers in the form of filepath:line
	logger.WithField("callers", logger.GetCallers(0, 5, false)).Write("Logging the call stack under field `callers`")

}
