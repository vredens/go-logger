.PHONY: test
.SILENT: coverage test bench

test:
	go test -v -race ./...

prof:
	mkdir .tmp
	go test -v -f ./.tmp/cpu.pprof -bench 'Bench' -benchmem .
	go tool pprof ./.tmp/cpu.pprof

bench:
	mkdir -p .benchmarks
	go test -cpu 1 -v -bench '^Bench' -run '^$$' -benchmem . -count=20 -timeout 60m | tee .benchmarks/all.out

bench-race:
	go test -v -bench '^Bench' -benchmem -race .

coverage:
	go test -coverprofile=.coverage.out -timeout 30s ./...
	go tool cover -func=.coverage.out

coverage-html:
	go tool cover -html=.coverage.out

clean:
	rm -f .coverage.out
	rm -rf vendor
