# v2.0.0

Split and restructured the Writer concept. Now there are three interfaces:

- Root
- Writer
- Formatter

## Breaking changes

- Writer interface is no longer the same thing, the old interface was replaced by the Root interface.
- WriterOption type was replaced by RootOption.
- NewWriter func is no longer what you expect, the old method must be converted simply to New() which returns a Root logger.
- WithRootFields func was converted to WithRootFields.
- WithRootTags func was converted to WithRootTags.
- WithOutput func is gone and must be replaced with WithWriter(NewWriter(io.Writer))

## Root logger

Combines a writer, a formatter, a minimum log level, root fields and tags.
The Root logger is the one responsible for spawning the actual loggers. It can also do basic log writing same as the old Writer interface.

## Writer

Writer is a breaking change and it now just has one responsibility, writing bytes to somewhere. It's only method is `Write([]byte])`. Default writers are provided which guarantee thread safety.

## Formatter

Formatter is what converts the four sections of a log event into a byte array. Default formatters exist for plain text and JSON.
