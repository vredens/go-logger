package logger

import (
	"fmt"
	"io"
	"os"
	"sync"
	"sync/atomic"
	"time"
)

// RootOption are options for all loggers implementing the Basic logger interface.
type RootOption func(*root)

type root struct {
	output    io.Writer
	mutex     sync.RWMutex
	debugMode uint32
	tags      []string
	fields    []keyval
	options   RootOptions
}

func (r *root) isDebugActive() bool {
	return atomic.LoadUint32(&r.debugMode) == 1
}

// DebugMode allows turning debug mode on/off. Only affects new Spawns.
func (r *root) DebugMode(enable bool) {
	if enable {
		atomic.StoreUint32(&r.debugMode, 1)
	} else {
		atomic.StoreUint32(&r.debugMode, 0)
	}
}

// Reconfigure ...
func (r *root) Reconfigure(opts ...RootOption) {
	r.mutex.Lock()
	defer r.mutex.Unlock()

	for _, o := range opts {
		o(r)
	}
}

// RootOptions is a set of micro-optimizations for the root logger. Use at your own risk.
type RootOptions struct {
	DedupFields bool
	DedupTags   bool
}

// ConfigRootOptions returns an option which sets a root logger's micro optimization options.
func ConfigRootOptions(opts RootOptions) RootOption {
	return func(r *root) {
		r.options = opts
	}
}

// ConfigWriter returns an option which sets a root logger's writer.
func ConfigWriter(w io.Writer) RootOption {
	return func(r *root) {
		r.output = w
	}
}

// ConfigRootTags option, a root logger has the tags provided replace its list of tags.
// A root logger's tags can not be changed or overridden by a logger and at Spawn time all tags are copied to the spawn.
func ConfigRootTags(tags ...string) RootOption {
	return func(r *root) {
		if len(tags) == 0 {
			return
		}
		r.tags = tags
	}
}

// ConfigRootFields option, a root logger has the provided fields replace its list of fields.
// A root logger's fields can not be changed or overridden by a logger and at Spawn time are copied to the spawn.
func ConfigRootFields(fields map[string]interface{}) RootOption {
	return func(r *root) {
		r.fields = make([]keyval, len(fields))
		var i int
		for k, v := range fields {
			r.fields[i] = newKV(k, v)
			i++
		}
	}
}

// New creates a new Root logger.
func New(opts ...RootOption) Root {
	return newRoot(opts...)
}

func newRoot(opts ...RootOption) *root {
	r := &root{
		output:    os.Stdout,
		debugMode: 0,
		mutex:     sync.RWMutex{},
		options: RootOptions{
			DedupFields: true,
			DedupTags:   true,
		},
	}

	r.Reconfigure(opts...)

	return r
}

func (r *root) writeError(msg string) {
	r.mutex.Lock()
	r.output.Write([]byte(fmt.Sprintf(`{"timestamp":"%s","msg":%q,"tags":["ERROR"]}`, time.Now().Format(time.RFC3339Nano), msg)))
	r.output.Write([]byte{'\n'})
	r.mutex.Unlock()
}

func (r *root) write(msg string, tags []string, fields, data []keyval) {
	enc := _jep.Get()
	defer _jep.Put(enc)

	enc.Write([]byte(`{"timestamp":`))
	ts, _ := time.Now().MarshalJSON()
	enc.Write(ts)
	enc.Write([]byte(`,"msg":`))
	enc.Encode(msg)

	if len(tags) > 0 {
		enc.Write([]byte(`,"tags":`))
		enc.Encode(tags)
	}

	if len(fields) > 0 {
		enc.Write([]byte(`,"fields":{`))
		enc.WriteKeyVals(fields)
		enc.WriteByte('}')
	}

	if len(data) > 0 {
		enc.Write([]byte(`,"data":{`))
		enc.WriteKeyVals(data)
		enc.WriteByte('}')
	}

	enc.Write([]byte(`}`))
	enc.WriteByte('\n')

	r.mutex.Lock()
	enc.buf.WriteTo(r.output)
	r.mutex.Unlock()
}

func (r *root) spawn() *logger {
	r.mutex.RLock()
	defer r.mutex.RUnlock()

	var l = &logger{
		root:   r,
		fields: r.fields,
		tags:   r.tags,
	}

	return l
}
