# Go-Logger v2.0

[![pipeline status](https://gitlab.com/vredens/go-logger/badges/master/pipeline.svg)](https://gitlab.com/vredens/go-logger/commits/master)
[![coverage report](https://gitlab.com/vredens/go-logger/badges/master/coverage.svg)](https://gitlab.com/vredens/go-logger/commits/master)
[![godoc](https://godoc.org/gitlab.com/vredens/go-logger?status.svg)](https://godoc.org/gitlab.com/vredens/go-logger)
[![goreportcard](https://goreportcard.com/badge/gitlab.com/vredens/go-logger)](https://goreportcard.com/report/gitlab.com/vredens/go-logger)

A logging library for Go.

Application logging:

* Should not be where your CPU spends most of the time.
* Should be done to STDERR/STDOUT. Log rotation, logging to log dispatchers or centralized log aggregators, etc should be the job of operations not development.
* Should not be confused with auditing, although it can be rudementarily used for it.
* Should not cause application failures, even if nothing is being logged due to an error in the logger.
* Should be simple to use in development and powerful enough to be used in production without convoluted and complex configurations. Id est, it should permit structured logging while keeping a simple interface.

## Requirements

Requires Go 1.9 or later.

## Log event structure

```
{
	"timestamp": <set by the logger at call time>,
	"message": "a message for someone else to understand",
	"tags": ["catalog","your","events","with","tags"],
	"fields": {
		... data structure to be used for filtering or aggregating log events ...
	},
	"data": {
		... a data structure with extra information useful for further debugging ...
	}
}
```

**Message** should be something aimed at second/third line support, specially in errors.

**Tags** is how you catalog events. A set of tags can be something like `["user", "create", "error"]` or `["consumer", "user_events", "too_many_requeues"]`.

**Fields** is a data structure aimed at filtering or aggregating logs. For example, if you want to find all log events related to a specific user you should include `user_id` in each log event under the fields key. This is the structured part of your log events and should be strong-typed and kept relatively small system wide. You should not re-use fields with different data types (a user_id as a number and on a different log event you add the fields.user_id as a string).

**Data** is unstructured information useful only for fine grained debugging.

## Scopes and package usage.

In an opinionated view of logging, one can consider a logger to have 4 hierarchical levels

* Application level is from where all loggers should be created.
* Component level is associated with only a part of the application. This is typically a domain package, library, "class", etc.
* Instance level should be associated with a data structure and respective methods, in the OOP world that would be a class instance. These loggers should be derived from the component level logger.
* Function level is specific to a certain function, with parts of the input or state included in the fields and/or tags.

## Quick Usage

### Download the package

If you are using Go modules you can install this with

```Bash
go get gitlab.com/vredens/go-logger@none
```

If you are using [Dep](https://github.com/golang/dep) you can simply add the package to your code and run `dep ensure` or install it in your project using

```Bash
dep ensure -add gitlab.com/vredens/go-logger
```

### Using it in your code

Check out [Examples](./example_test.go)
