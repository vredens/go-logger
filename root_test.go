package logger

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var (
	errExample = errors.New("fail")

	_messages   = fakeMessages(1000)
	_tenInts    = []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}
	_tenStrings = []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"}
	_tenTimes   = []time.Time{
		time.Unix(0, 0),
		time.Unix(1, 0),
		time.Unix(2, 0),
		time.Unix(3, 0),
		time.Unix(4, 0),
		time.Unix(5, 0),
		time.Unix(6, 0),
		time.Unix(7, 0),
		time.Unix(8, 0),
		time.Unix(9, 0),
	}
	_oneUser = &user{
		Name:      "Jane Doe",
		Email:     "jane@test.com",
		CreatedAt: time.Date(1980, 1, 1, 12, 0, 0, 0, time.UTC),
	}
	_tenUsers = users{
		_oneUser,
		_oneUser,
		_oneUser,
		_oneUser,
		_oneUser,
		_oneUser,
		_oneUser,
		_oneUser,
		_oneUser,
		_oneUser,
	}
)

func fakeMessages(n int) []string {
	messages := make([]string, n)
	for i := range messages {
		messages[i] = fmt.Sprintf("Test logging, but use a somewhat realistic message length. (#%v)", i)
	}
	return messages
}

func getMessage(iter int) string {
	return _messages[iter%1000]
}

type users []*user

type user struct {
	Name      string    `json:"name"`
	Email     string    `json:"email"`
	CreatedAt time.Time `json:"created_at"`
}

type LogEntry struct {
	Msg    string                 `json:"msg"`
	Tags   []string               `json:"tags"`
	Fields map[string]interface{} `json:"fields"`
	Data   map[string]interface{} `json:"data"`
}

func fakeGoLoggerFields() map[string]interface{} {
	return map[string]interface{}{
		"int":     _tenInts[0],
		"ints":    _tenInts,
		"string":  _tenStrings[0],
		"strings": _tenStrings,
		"time":    _tenTimes[0],
		"times":   _tenTimes,
		"user1":   _oneUser,
		"user2":   _oneUser,
		"users":   _tenUsers,
		"error":   errExample,
	}
}

func fakeFields() map[string]interface{} {
	mydata := struct {
		Name string `json:"name"`
	}{
		Name: "my name",
	}
	j, _ := json.Marshal(mydata)

	return map[string]interface{}{
		"data":  "complicated",
		"int":   1,
		"float": 0.15123,
		"struct": struct {
			Name string
		}{
			Name: "something",
		},
		"json": string(j),
	}
}

func fakeData() map[string]interface{} {
	mydata := struct {
		Name string `json:"name"`
	}{
		Name: "my name",
	}
	j, _ := json.Marshal(mydata)

	return map[string]interface{}{
		"data":  "complicated",
		"int":   1,
		"float": 0.15123,
		"at":    GetCaller(1, false),
		"struct": struct {
			Name string
		}{
			Name: "something",
		},
		"json": string(j),
	}
}

var stringerFields = map[string]interface{}{
	"name":      "fields",
	"type":      "tringer",
	"id":        "yes",
	"are cool?": false,
}

var optCoreFields = ConfigRootFields(fakeGoLoggerFields())
var optDiscard = ConfigWriter(ioutil.Discard)

func newTestWriter(opts ...RootOption) (*root, *bytes.Buffer) {
	b := &bytes.Buffer{}
	w := newRoot(ConfigWriter(b))
	w.Reconfigure(opts...)

	return w, b
}

func TestWrite(t *testing.T) {
	w, b := newTestWriter(ConfigRootFields(fakeFields()))
	w.Spawn().Write("simple message")
	assert.Contains(t, b.String(), `"simple message"`)
	assert.Contains(t, b.String(), `"data":"complicated"`)
	assert.Contains(t, b.String(), `"float":0.15123`)
	assert.Contains(t, b.String(), `"int":1`)
	assert.Contains(t, b.String(), `"json":"{\"name\":\"my name\"}"`)
	assert.Contains(t, b.String(), `"struct":{"Name":"something"}`)

	w, b = newTestWriter(ConfigRootFields(nil))
	w.Spawn().Write("simple message")
	assert.NotContains(t, b.String(), `[data:complicated]`)
	assert.Contains(t, b.String(), `simple message`)
}

func TestWriteDataWithFields(t *testing.T) {
	w, b := newTestWriter(ConfigRootFields(fakeFields()))
	w.Spawn().WithData(fakeData()).Write("some values")
	assert.Contains(t, b.String(), `"some values"`)
	assert.Contains(t, b.String(), `"data":"complicated"`)
	assert.Contains(t, b.String(), `"float":0.15123`)
	assert.Contains(t, b.String(), `"int":1`)
	assert.Contains(t, b.String(), `"json":"{\"name\":\"my name\"}"`)
	assert.Contains(t, b.String(), `"struct":{"Name":"something"}`)
	assert.Contains(t, b.String(), `"at":"root_test.go:174"`)
	assert.Contains(t, b.String(), `"data":"complicated"`)
	assert.Contains(t, b.String(), `"float":0.15123`)
	assert.Contains(t, b.String(), `"int":1`)
	assert.Contains(t, b.String(), `"json":"{\"name\":\"my name\"}"`)
	assert.Contains(t, b.String(), `"struct":{"Name":"something"}`)
}

func TestWriteDataWithFieldsAndTags(t *testing.T) {
	w, b := newTestWriter(ConfigRootFields(fakeFields()), ConfigRootTags("test", "tags", "fields"))
	w.Spawn().WithData(fakeData()).Write("some values")
	assert.Contains(t, b.String(), `"tags":["test","tags","fields"]`)
	assert.Contains(t, b.String(), `"msg":"some values"`)
	assert.Contains(t, b.String(), `"data":"complicated"`)
	assert.Contains(t, b.String(), `"float":0.15123`)
	assert.Contains(t, b.String(), `"int":1`)
	assert.Contains(t, b.String(), `"json":"{\"name\":\"my name\"}"`)
	assert.Contains(t, b.String(), `"struct":{"Name":"something"}`)
	assert.Contains(t, b.String(), `"at":"root_test.go:191"`)
	assert.Contains(t, b.String(), `"data":"complicated"`)
	assert.Contains(t, b.String(), `"float":0.15123`)
	assert.Contains(t, b.String(), `"int":1`)
	assert.Contains(t, b.String(), `"json":"{\"name\":\"my name\"}"`)
	assert.Contains(t, b.String(), `"struct":{"Name":"something"}`)
}

func TestWriterAsJSON(t *testing.T) {
	w, b := newTestWriter(optCoreFields)
	w.Spawn().Write("simple message")
	assert.Contains(t, b.String(), `"msg":"simple message"`)
	// TODO: JSON unmarshal and compare fields

	b.Reset()

	w.Spawn().Write("simple message")
	assert.Contains(t, b.String(), `"msg":"simple message"`)
	// TODO: JSON unmarshal and compare fields

	b.Reset()

	w2 := New(optCoreFields, ConfigRootTags("test", "tags"), ConfigWriter(b))
	w2.Spawn().WithData(fakeFields()).Write("some values")
	assert.Contains(t, b.String(), `"msg":"some values"`)
	assert.Contains(t, b.String(), `"data":"complicated"`)
	assert.Contains(t, b.String(), `"float":0.15123`)
	assert.Contains(t, b.String(), `"int":1`)
	assert.Contains(t, b.String(), `"json":"{\"name\":\"my name\"}"`)
	assert.Contains(t, b.String(), `"struct":{"Name":"something"}`)
	assert.Contains(t, b.String(), `"tags":["test","tags"]`)
	// TODO: JSON unmarshal and compare fields

	b.Reset()

	w3 := newRoot(optCoreFields, ConfigRootTags("test", "tags"), ConfigWriter(b))
	w3.write("some values", nil, nil, nil)
	assert.Contains(t, b.String(), `"msg":"some values"`)
	assert.NotContains(t, b.String(), `"data":"complicated"`)
	assert.NotContains(t, b.String(), `"float":0.15123`)
	assert.NotContains(t, b.String(), `"int":1`)
	assert.NotContains(t, b.String(), `"json":"{\"name\":\"my name\"}"`)
	assert.NotContains(t, b.String(), `"struct":{"Name":"something"}`)
	assert.NotContains(t, b.String(), `"tags":["test","tags"]`)

	b.Reset()

	w4 := newRoot(optCoreFields, ConfigRootTags("test", "tags"), ConfigWriter(b))
	w4.write("some values", nil, nil, []keyval{
		newKV("error", make(chan int)),
	})
	assert.Contains(t, b.String(), `"_error":"there was a problem formatting field: json: unsupported type: chan int"`)
}

func TestRootWrite(t *testing.T) {
	var out LogEntry
	var res = &bytes.Buffer{}
	var writer = newRoot(ConfigWriter(res))
	var helperMapToFieldSlice = func(fields map[string]interface{}) []keyval {
		var out = make([]keyval, 0, len(fields))
		for k, v := range fields {
			out = append(out, newKV(k, v))
		}

		return out
	}

	msg := `something @ł€¶ŧ←↓→""øþæßð«»¢đ“ü`
	tags := []string{"¹@£", "@ł€", "æßð", "«»¢¡⅛£"}
	fields := KV{
		"coiso":    "coiso",
		"cenas":    "cenas",
		"algalias": float64(100),
	}
	fieldSlice := helperMapToFieldSlice(fields)
	data := KV{
		"coiso":    "coiso",
		"cenas":    "cenas",
		"algalias": float64(100),
	}

	writer.write(msg, tags, fieldSlice, helperMapToFieldSlice(data))
	out = LogEntry{}
	if err := json.Unmarshal(res.Bytes(), &out); err != nil {
		t.Errorf("unmarshal failed: %s [%s]", err, string(res.Bytes()))
	}
	assert.ElementsMatch(t, tags, out.Tags)
	assert.Equal(t, fields, out.Fields)
	assert.Equal(t, data, out.Data)
	assert.Equal(t, msg, out.Msg)
}

func TestWriterConcurrency(t *testing.T) {
	var wg sync.WaitGroup

	w, _ := newTestWriter()
	l := w.Spawn()

	for i := 0; i < 5; i++ {
		wg.Add(1)
		data := map[string]interface{}{
			"worker": i,
		}
		go func() {
			l.WithData(data).Debug().Write("worker")
			wg.Done()
		}()
	}
	wg.Wait()
}

func BenchmarkBasicWriteDataForZapComparison(b *testing.B) {
	b.Run("Text/Write", func(b *testing.B) {
		log := New(optCoreFields, optDiscard)
		b.ResetTimer()
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				log.Spawn().Write("some values")
			}
		})
	})

	b.Run("Text/WriteData", func(b *testing.B) {
		log := New(optCoreFields, optDiscard)
		b.ResetTimer()
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				log.Spawn().WithData(fakeGoLoggerFields()).Write("some values")
			}
		})
	})

	b.Run("Text/Fields/Write", func(b *testing.B) {
		log := New(optCoreFields, optDiscard)
		b.ResetTimer()
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				log.Spawn().Write("some values")
			}
		})
	})
}

func BenchmarkData(b *testing.B) {
	b.Run("WithData", func(b *testing.B) {
		rlog := New(optCoreFields, optDiscard)
		log := rlog.Spawn()
		b.ResetTimer()
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				log.WithData(fakeGoLoggerFields()).Write("some values")
			}
		})
	})

	b.Run("Ctx/WithData", func(b *testing.B) {
		rlog := New(optCoreFields, optDiscard)
		log := rlog.Spawn().WithData(fakeGoLoggerFields())
		b.ResetTimer()
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				log.Write("some values")
			}
		})
	})
}

func BenchmarkFields(b *testing.B) {
	b.Run("WithFields", func(b *testing.B) {
		rlog := New(optCoreFields, optDiscard)
		log := rlog.Spawn()
		b.ResetTimer()
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				log.WithFields(fakeGoLoggerFields()).Write("some values")
			}
		})
	})

	b.Run("Ctx/WithFields", func(b *testing.B) {
		rlog := New(optCoreFields, optDiscard)
		log := rlog.Spawn().WithFields(fakeGoLoggerFields())
		b.ResetTimer()
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				log.Write("some values")
			}
		})
	})
}
