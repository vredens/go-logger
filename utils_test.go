package logger

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUtilGetCaller(t *testing.T) {
	var str string

	str = GetCaller(0, false)
	assert.Equal(t, "utils_test.go:13", str)

	cwd, _ := os.Getwd()

	str = GetCaller(0, true)
	assert.Equal(t, cwd+"/utils_test.go:18", str)
}

func TestUtilCallStackInfo(t *testing.T) {
	a := func() {
		var callers = GetCallers(0, 10, false)
		assert.Contains(t, callers[0], "utils_test.go:24")
		assert.Contains(t, callers[1], "utils_test.go:30")
	}

	b := func() {
		a()
	}

	b()

	var callers = GetCallers(0, 10, false)
	assert.Contains(t, callers[0], "utils_test.go:35")
}
