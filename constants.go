package logger

const (
	// PRIVATE log tag should be used to mark log events which dump large amounts of private and unstructured data. These are typically useful in test environments but centralized logging infrastructures should anonymize or drop these events.
	PRIVATE = "PRIVATE"
	// RAW log tag can be used to mark log events which dump large amounts of unstructured data.
	RAW = "RAW"
	// TRACING tag can be used to mark debug log events. This is different from the debug mode.
	TRACING = "TRACING"
	// INFO tag can be used to mark informative log events. For example, informing that a database connection was established, a message was received, etc. This should be used for log analysis, metrics and basic profiling of bottlenecks.
	INFO = "INFO"
	// WARN log tag should be used in situations where many entries of this type should trigger an allert in your centralized logging system. It only makes sense if you plan to use such a system architecture.
	WARN = "WARN"
	// ERROR log tag should be used in all error situations. Careful with what you consider an error. Typically if you are handling the situation or returning the error to the caller, you should not mark it as an ERROR.
	ERROR = "ERROR"
	// ALERT log tag should be used when a single log event should trigger an alert.
	ALERT = "ALERT"
)
