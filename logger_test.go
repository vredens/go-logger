package logger

import (
	"bytes"
	"os"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInitializeLoggers(t *testing.T) {
	w, b := newTestWriter()

	l1 := w.Spawn()
	l1.Write("here")
	assert.Contains(t, b.String(), "here")

	b.Reset()

	l3 := l1.WithTags()
	l3.Write("here3")
	assert.Contains(t, b.String(), "here3")

	b.Reset()
}

func TestTagging(t *testing.T) {
	w, b := newTestWriter()
	l := w.Spawn()
	w.DebugMode(true)

	l1 := l.WithTags("ONE")
	l2 := l1.Debug().WithTags("TWO")

	l1.WithTags("OOPS").Write("a")
	assert.Contains(t, b.String(), "OOPS")
	b.Reset()

	l2.Write("b")
	assert.Contains(t, b.String(), "TWO")
	assert.NotContains(t, b.String(), "OOPS")
	b.Reset()

	l1.WithTags("OOPS").Write("a")
	assert.Contains(t, b.String(), "OOPS")
	b.Reset()

	l2.Write("b")
	assert.Contains(t, b.String(), "TWO")
	assert.NotContains(t, b.String(), "OOPS")
	b.Reset()
}

func TestLoggingSimpleMessage(t *testing.T) {
	w, b := newTestWriter()
	l := w.Spawn()

	w.DebugMode(true)
	l.Debug().Write("debug")
	assert.Contains(t, b.String(), `debug`)
	b.Reset()

	w.DebugMode(false)
	l.Write("info")
	assert.Contains(t, b.String(), `info`)
	b.Reset()
}

func TestWriterReconfiguration(t *testing.T) {
	w, b := newTestWriter()
	w.DebugMode(true)
	l := w.Spawn()
	l.Debug().Write("sample")
	checkBuffer(t, b)
	assert.Contains(t, b.String(), "sample")

	b.Reset()

	w.DebugMode(false)
	l.Debug().Write("sample")
	assert.Equal(t, b.String(), "")

	b2 := &bytes.Buffer{}
	w.Reconfigure(ConfigWriter(b2))

	l.Write("sample")
	checkBuffer(t, b2)
	assert.Equal(t, b.String(), "")
	assert.Contains(t, b2.String(), "sample")

	b2.Reset()

	// fields are immutable once a logger has been created
	w.Reconfigure(ConfigRootFields(map[string]interface{}{
		"test": "test",
	}))

	l.Write("sample")
	checkBuffer(t, b2)
	assert.Contains(t, b2.String(), "sample")
	assert.NotContains(t, b2.String(), `test="test"`)
}

func TestFieldsAndSpawnsOfLogger(t *testing.T) {
	w, b := newTestWriter()

	// fields are immutable once a logger has been created
	l := w.Spawn().WithFields(map[string]interface{}{
		"test": "test",
	})

	l.Write("sample")
	checkBuffer(t, b)
	assert.Contains(t, b.String(), `"msg":"sample"`)
	assert.Contains(t, b.String(), `"test":"test"`)

	b.Reset()

	// fields are immutable once a logger has been created
	l2 := l.WithFields(map[string]interface{}{
		"test-spawn": "test",
	})

	l2.Write("sample")
	checkBuffer(t, b)
	assert.Contains(t, b.String(), `"msg":"sample"`)
	assert.Contains(t, b.String(), `"test":"test"`)
	assert.Contains(t, b.String(), `"test-spawn":"test"`)
}

func TestTagsAndSpawnsOfLogger(t *testing.T) {
	w, b := newTestWriter(ConfigRootTags("immutable"))
	l := w.Spawn().WithTags("hello")

	l.Write("sample")
	assert.Contains(t, b.String(), `"tags":["immutable","hello"]`)
	assert.Contains(t, b.String(), `"msg":"sample"`)

	b.Reset()

	w.Reconfigure(ConfigRootTags("extra"))
	l.Write("sample")
	checkBuffer(t, b)
	assert.Contains(t, b.String(), `immutable`)
	assert.Contains(t, b.String(), `hello`)
	assert.NotContains(t, b.String(), `extra`)

	b.Reset()

	l2 := w.Spawn()
	l2.Write("sample")
	checkBuffer(t, b)
	assert.NotContains(t, b.String(), `immutable`)
	assert.NotContains(t, b.String(), "hello")
	assert.Contains(t, b.String(), `extra`)

	b.Reset()

	l2 = l.WithTags("goodbye")
	l2.Write("sample #2")
	checkBuffer(t, b)
	assert.Contains(t, b.String(), `immutable`)
	assert.Contains(t, b.String(), `hello`)
	assert.Contains(t, b.String(), `goodbye`)
}

func TestLoggerInterfaceMethods(t *testing.T) {
	w, b := newTestWriter()
	l := w.Spawn().WithField("field", "val")

	l2 := w.Spawn().WithFields(KV{"component": "test"}).WithTags("one", "two")
	l3 := l2.WithFields(KV{"service": "test", "component": "test-2"})
	l3.WithData(KV{"xpto": "amen"}).Write("stuff")
	assert.Contains(t, b.String(), `"xpto":"amen"`)
	b.Reset()

	w.DebugMode(true)
	l.Debug().Write("test")
	assert.Contains(t, b.String(), `test`)
	b.Reset()
	l.Debug().Write("test %s", "a")
	assert.Contains(t, b.String(), `test a`)
	b.Reset()
	l.WithData(KV{"test": "field"}).Debug().Write("test")
	assert.Contains(t, b.String(), `"msg":"test"`)
	assert.Contains(t, b.String(), `"test":"field"`)
	assert.Contains(t, b.String(), `"field":"val"`)
	b.Reset()

	w.DebugMode(false)
	l.Debug().Write("test")
	assert.NotContains(t, b.String(), `test`)
	l.Write("test")
	assert.Contains(t, b.String(), `test`)
	b.Reset()
	l.Write("test %s", "a")
	assert.Contains(t, b.String(), `test a`)
	b.Reset()
	l.WithData(KV{"test": "field"}).Write("test")
	assert.Contains(t, b.String(), `"msg":"test"`)
	assert.Contains(t, b.String(), `"test":"field"`)
	assert.Contains(t, b.String(), `"field":"val"`)
	b.Reset()
}

func TestLoggerDebugModeChecking(t *testing.T) {
	w, b := newTestWriter()
	l := w.spawn().WithTags("ttag")

	w.DebugMode(true)
	l.Debug().Write("ok")
	assert.Contains(t, b.String(), `ok`)
	assert.Contains(t, b.String(), `["ttag"]`)
	b.Reset()
	l.Write("ok")
	assert.Contains(t, b.String(), `ok`)
	assert.Contains(t, b.String(), `["ttag"]`)
	b.Reset()

	w.spawn().Debug().Write("a test")
	assert.Contains(t, b.String(), `a test`)
	b.Reset()

	w.DebugMode(false)
	l.Debug().Write("test")
	l.Debug().Write("test %s", "a")
	l.Debug().WithData(KV{"test": "field"}).Write("test")
	assert.Empty(t, b.String())
	l.Write("ok")
	assert.Contains(t, b.String(), `ok`)
	b.Reset()

	w.spawn().Debug().Write("a test")
	assert.Empty(t, b.String())
	b.Reset()

}

func TestEntryLogger(t *testing.T) {
	w, b := newTestWriter(ConfigRootTags("immutable"))
	var l = w.Spawn().WithTags("hello").WithFields(KV{"field": `"field-value"`})

	l.WithData(KV{"data": `"vd"ata"`}).Write("sample")
	assert.Contains(t, b.String(), `immutable`)
	assert.Contains(t, b.String(), `hello`)
	assert.Contains(t, b.String(), `"field":"\"field-value\""`)
	assert.Contains(t, b.String(), `"data":"\"vd\"ata\""`)

	b.Reset()

	l.WithData(KV{"data": "vdata"}).WithData(KV{"new": "stuff"}).Write("sample")
	assert.Contains(t, b.String(), `"data":"vdata"`)
	assert.Contains(t, b.String(), `"new":"stuff"`)

	b.Reset()

	var _l = l.WithTags("tag1").WithTags("tag2")
	_l.Write("sample")
	assert.Contains(t, b.String(), `["immutable","hello","tag1","tag2"]`)

	b.Reset()

	l1 := l.WithTags("tag1", "tag2").WithFields(KV{"key1": "val1", "key2": "val2"})
	l1.Write("info")
	assert.Contains(t, b.String(), `["immutable","hello","tag1","tag2"]`)
	assert.Contains(t, b.String(), `"msg":"info"`)
	assert.Contains(t, b.String(), `"key1":"val1"`)
	assert.Contains(t, b.String(), `"key2":"val2"`)

	b.Reset()

	l2 := l.WithFields(KV{"key1": "val1", "key2": "val2"})
	l2.Write("info")
	assert.Contains(t, b.String(), `"tags":["immutable","hello"]`)
	assert.Contains(t, b.String(), `"msg":"info"`)
	assert.Contains(t, b.String(), `"key1":"val1"`)
	assert.Contains(t, b.String(), `"key2":"val2"`)

	b.Reset()

	l.WithData(KV{"data": "vdata"}).Write("sample")
	assert.Contains(t, b.String(), `"immutable"`)
	assert.Contains(t, b.String(), `"hello"`)
	assert.Contains(t, b.String(), `"field":"\"field-value\""`)
	assert.Contains(t, b.String(), `"data":"vdata"`)
	assert.NotContains(t, b.String(), `"key1":"val1"`)
	assert.NotContains(t, b.String(), `"key2":"val2"`)
}
func TestLogConcurrent(t *testing.T) {
	var wg sync.WaitGroup

	w, _ := newTestWriter()
	l := w.Spawn()

	for i := 0; i < 2; i++ {
		wg.Add(1)
		data := map[string]interface{}{
			"worker": i,
		}
		go func() {
			l.WithData(data).Debug().Write("worker")
			wg.Done()
		}()
	}
	wg.Wait()
}

func TestSpawnConcurrency(t *testing.T) {
	var wg sync.WaitGroup

	w, _ := newTestWriter()
	l := w.spawn()

	for i := 0; i < 5; i++ {
		wg.Add(1)
		data := map[string]interface{}{
			"worker": i,
		}
		go func() {
			l.spawn().WithFields(data)
			l.WithFields(data).Write("test")
			wg.Done()
		}()
	}
	wg.Wait()
}

var bs []byte

func BenchmarkLoggerSpawning(b *testing.B) {
	b.Run("Spawn/WithTags", func(b *testing.B) {
		w := New(optCoreFields, optDiscard)
		b.ResetTimer()
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				w.Spawn().WithTags("some", "cool", "tags", "three", "are", "enough", "oh", "wait")
			}
		})
	})

	b.Run("Spawn/WithFields[strings]", func(b *testing.B) {
		w := New(optDiscard)
		b.ResetTimer()
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				w.Spawn().WithFields(stringerFields)
			}
		})
	})

	b.Run("Spawn/With[Fields,Data,Tags]", func(b *testing.B) {
		w := New(optDiscard)
		b.ResetTimer()
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				w.Spawn().WithFields(fakeGoLoggerFields()).WithData(fakeGoLoggerFields()).WithTags("some", "cool", "tags", "three", "are", "enough", "oh", "wait")
			}
		})
	})
}

func BenchmarkCompareDisabled(b *testing.B) {
	log := New(optDiscard).Spawn().Debug()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		var i int
		for pb.Next() {
			log.Write(getMessage(i))
			i++
		}
	})
}

func BenchmarkCompareDisabledWithContext(b *testing.B) {
	log := New(optDiscard).Spawn().WithFields(generateSampleFields()).Debug()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		var i int
		for pb.Next() {
			log.Write(getMessage(i))
			i++
		}
	})
}

func BenchmarkCompareDisabledWithFields(b *testing.B) {
	log := New(optDiscard).Spawn().Debug()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		var i int
		for pb.Next() {
			log.WithFields(generateSampleFields()).Write(getMessage(i))
			i++
		}
	})
}

func BenchmarkCompareSimple(b *testing.B) {
	log := New(optDiscard).Spawn()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		var i int
		for pb.Next() {
			log.Write(getMessage(i))
			i++
		}
	})
}

func BenchmarkCompareWithFields(b *testing.B) {
	b.Run("WithFields", func(b *testing.B) {
		log := New(optDiscard).Spawn()
		b.ResetTimer()
		b.RunParallel(func(pb *testing.PB) {
			var i int
			for pb.Next() {
				log.WithFields(fakeGoLoggerFields()).Write(getMessage(i))
				i++
			}
		})
	})
	b.Run("WithField", func(b *testing.B) {
		log := New(optDiscard).Spawn()
		b.ResetTimer()
		b.RunParallel(func(pb *testing.PB) {
			var i int
			for pb.Next() {
				log.
					WithField("int", _tenInts[0]).
					WithField("ints", _tenInts).
					WithField("string", _tenStrings[0]).
					WithField("strings", _tenStrings).
					WithField("time", _tenTimes[0]).
					WithField("times", _tenTimes).
					WithField("user1", _oneUser).
					WithField("user2", _oneUser).
					WithField("users", _tenUsers).
					WithField("error", errExample).
					Write(getMessage(i))
				i++
			}
		})
	})
}

func BenchmarkCompareWithContext(b *testing.B) {
	log := New(optDiscard).Spawn().WithFields(fakeGoLoggerFields())
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		var i int
		for pb.Next() {
			log.Write(getMessage(i))
			i++
		}
	})
}

func BenchmarkCompareWithFieldsTwoLogs(b *testing.B) {
	log := New(optDiscard).Spawn()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			var log = log.WithFields(fakeGoLoggerFields())
			log.Write(getMessage(0))
			log.Write(getMessage(1))
		}
	})
}

func BenchmarkCompareOverall(b *testing.B) {
	log := New(optDiscard).Spawn().
		WithTags("one", "two").
		WithField("one", "first field").
		WithField("two", 200).
		WithData(KV{"one": "first data", "two": 100})
	debugLog := log.Debug()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		var i int
		for pb.Next() {
			var debugLog = debugLog.WithTags("smple", "tags").
				WithFields(KV{"user": _oneUser, "value": 4123.123}).
				WithData(KV{"raw": _oneUser})
			var log = log.
				WithTags("smple", "tags").
				WithFields(KV{"user": _oneUser, "value": 4123.123}).
				WithData(KV{"raw": _oneUser})
			log.Write(getMessage(i))
			log.Write(getMessage(i + 50))
			debugLog.Write(getMessage(i + 1))
			debugLog.Write(getMessage(i + 25))
			debugLog.Write(getMessage(i + 37))
			i++
		}
	})
}

var optDebugDiscard = ConfigWriter(os.Stdout)

func checkBuffer(t *testing.T, b *bytes.Buffer) {
	// t.Helper()
	if b.Len() == 0 {
		t.Log("nothing was logged")
		t.FailNow()
	}
	b.Truncate(b.Len() - 1) // remove trailing \n
}

func generateSampleFields() map[string]interface{} {
	return map[string]interface{}{
		"str":  "test",
		"strs": []string{"a", "b", "c"},
		"int":  100,
		"ints": []int{100, 200, 300},
		"struct": struct {
			Name string
			Age  int
		}{
			Name: "ze",
			Age:  20,
		},
	}
}
