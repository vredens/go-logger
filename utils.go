package logger

import (
	"fmt"
	"path"
	"runtime"
)

// GetCaller returns a single "filename:line function" string of the caller at the provided depth `depth`.
// If fullPath is set to true then the file's full path is used otherwise only its name.
func GetCaller(depth int, fullPath bool) string {
	_, file, line, _ := runtime.Caller(depth + 1)
	if fullPath {
		return fmt.Sprintf("%s:%d", file, line)
	}
	return fmt.Sprintf("%s:%d", path.Base(file), line)
}

// GetCallers is a helper for returning a list of "filename:line function" of the callers of this function skips `from` and adds up to `size` initial callers.
func GetCallers(from, size int, fullPath bool) []string {
	if size == 0 {
		size = 1
	}
	from += 2 // because runtime.Callers always requires 2 to be skipped

	pc := make([]uintptr, size)
	runtime.Callers(from, pc)
	frames := runtime.CallersFrames(pc)
	var callers = make([]string, 0, size)
	for f, n := frames.Next(); n; f, n = frames.Next() {
		if fullPath {
			callers = append(callers, fmt.Sprintf("%s [%s:%d]", f.Function, f.File, f.Line))
		} else {
			callers = append(callers, fmt.Sprintf("%s [%s:%d]", f.Function, path.Base(f.File), f.Line))
		}
	}

	return callers
}
