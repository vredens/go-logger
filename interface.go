package logger

// KV is a type alias for map[string]interface{}.
type KV = map[string]interface{}

// Tags is a type alias for []string.
type Tags = []string

// Root is the actual encoder and writer of log entries and you should create one at the start of your application but
// you probably won't use it much after the first Spawn().
// To generate log entries you need to Spawn() a Logger.
type Root interface {
	// Spawn creates a new Logger associated with the root logger.
	Spawn() Logger
	// Reconfigure allows to reconfigure certain aspects of the Root logger.
	Reconfigure(...RootOption)
	// DebugMode allows turning debug mode on/off.
	DebugMode(enable bool)
}

// Logger is a typical logger interface which you should use to generate log entries which the Root logger will then
// encode (JSON) and write to the defined output Writer.
type Logger interface {
	// Debug marks the logger as in debug mode.
	Debug() Logger
	// WithTags creates a new logger and adds the provided tags, skipping any tag that already exists.
	WithTags(tags ...string) Logger
	// WithFields spawns a new logger adding the provided key value map as fields, skipping any field already present.
	WithFields(fields map[string]interface{}) Logger
	// WithField creates a new logger and adds the provided field. This is a faster method with fewer allocation when adding only 1 field.
	WithField(key string, value interface{}) Logger
	// WithData spawns a logger then adds the provided data.
	WithData(data map[string]interface{}) Logger
	// Write writes a log entry using the logger's current tags, fields and data.
	Write(format string, args ...interface{})
}
