package logger

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var _kv keyval
var benchByteSliceOutput1 []byte

func TestNewKeyVal(t *testing.T) {
	var kv keyval

	kv = newKV("test", `"te":"st"`)
	assert.Equal(t, []byte(`"\"te\":\"st\""`), kv.data, "format failed for quoted string")

	kv = newKV("test", `\"te":\"st"`)
	assert.Equal(t, []byte(`"\\\"te\":\\\"st\""`), kv.data, "format failed for quoted string type 2")

	kv = newKV("test", []string{`"a"`, `b`})
	assert.Equal(t, []byte(`["\"a\"","b"]`), kv.data, "format failed for array: %s")

	kv = newKV("test", 123)
	assert.Equal(t, []byte(`123`), kv.data, "format failed for number")

	kv = newKV("test", "{an amazing test of power}")
	assert.Equal(t, []byte(`"{an amazing test of power}"`), kv.data, "format failed for string with special chars")

	kv = newKV("test", "an amazing test of ultra power")
	assert.Equal(t, []byte(`"an amazing test of ultra power"`), kv.data, "format failed for quoted string")

	kv = newKV("test", "")
	assert.Equal(t, []byte(`""`), kv.data, "failed for empty string")

	kv = newKV("test", "aþø→")
	assert.Exactly(t, []byte(`"aþø→"`), kv.data)

	kv = newKV("test", 1)
	assert.Exactly(t, []byte(`1`), kv.data)

	kv = newKV("test", 10.2312341)
	assert.Exactly(t, []byte(`10.2312341`), kv.data)

	var sample = struct {
		Name   string
		Number int
	}{
		Name:   "john",
		Number: 1,
	}
	kv = newKV("test", sample)
	assert.Exactly(t, []byte(`{"Name":"john","Number":1}`), kv.data)
}

func BenchmarkNewKeyVal(b *testing.B) {
	var str string
	for i := 0; i < 10; i++ {
		str = str + "test "
	}
	b.Run("encoder", func(b *testing.B) {
		jenc := _jep.Get()
		b.ResetTimer()
		for k := 0; k < b.N; k++ {
			jenc.Encode(str)
			benchByteSliceOutput1 = jenc.Done()
			jenc.buf.Reset()
		}
	})
	b.Run("String", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			_kv = newKV("test", str)
		}
	})
	b.Run("StringArray", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			_kv = newKV("test", []string{"a", "b", "c"})
		}
	})
	b.Run("Int", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			_kv = newKV("test", 100)
		}
	})
	b.Run("IntArray", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			_kv = newKV("test", []int{100, 200, 300})
		}
	})
	b.Run("Float", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			_kv = newKV("test", 100.98)
		}
	})
	b.Run("Struct", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			_kv = newKV("test", struct {
				Name string
				Age  int
			}{
				Name: "ze",
				Age:  20,
			})
		}
	})
}
