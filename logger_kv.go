package logger

import "encoding/json"

// keyval is a struct identifying a log entry's single structured key-value.
type keyval struct {
	name string
	data []byte
}

// newKV helper for constructing keyval. This will pre-format the value.
func newKV(key string, value interface{}) keyval {
	data, err := json.Marshal(value)
	if err != nil {
		return keyval{name: "_" + key, data: []byte(`"there was a problem formatting field: ` + err.Error() + `"`)}
	}
	return keyval{name: key, data: data}
}
