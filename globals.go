package logger

import "io/ioutil"

var gRoot = newRoot()
var gDummy = dummy{}

// Spawn creates a new Logger directly from the global log book.
func Spawn() Logger {
	return gRoot.spawn()
}

// SpawnMute creates a new logger which implements the Logger interface but blocks all logging. This is simply a helper in order to inject loggers into components to keep them quiet.
func SpawnMute() Logger {
	return New(ConfigWriter(ioutil.Discard)).Spawn()
}

// Reconfigure the root logger.
func Reconfigure(opts ...RootOption) {
	gRoot.Reconfigure(opts...)
}

// WithTags spawns a new log entry from the global logger with the provided tags.
func WithTags(tags ...string) Logger {
	return gRoot.spawn().WithTags(tags...)
}

// WithFields spawns a new log entry from the global logger with the provided fields.
func WithFields(fields map[string]interface{}) Logger {
	return gRoot.spawn().WithFields(fields)
}

// WithField spawns a new log entry from the global logger with the provided list of Field's.
func WithField(key string, value interface{}) Logger {
	return gRoot.spawn().WithField(key, value)
}

// WithData spawns a new log entry from the global logger with the provided data.
func WithData(data map[string]interface{}) Logger {
	return gRoot.spawn().WithData(data)
}

// DebugMode allows turning debug mode on/off. Only affects new spawns.
func DebugMode(enable bool) {
	gRoot.DebugMode(enable)
}

// Debug spawns a new logger for debugging.
func Debug() Logger {
	return gRoot.spawn().Debug()
}

// Write a log entry.
func Write(fmt string, args ...interface{}) {
	gRoot.spawn().Write(fmt, args...)
}
